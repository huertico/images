===========
repo-images
===========

Shared images to be referenced on README files.

  .. image:: earth.png
     :target: https://gitlab.com/huertico/sensors
     :alt: eartch

  .. image:: fire.png
     :target: https://gitlab.com/huertico
     :alt: fire

  .. image:: metal.png
     :target: https://gitlab.com/huertico/admin
     :alt: metal

  .. image:: water.png
     :target: https://gitlab.com/huertico/watering
     :alt: water

  .. image:: wind.png
     :target: https://gitlab.com/huertico/server
     :alt: wind

  .. image:: library.png
     :target: https://gitlab.com/huertico/huertico-library
     :alt: library
     

-----
Logos
-----

  .. image:: tomato-blue.png
     :alt: blue tomato

  .. image:: tomato-green.png
     :alt: green tomato

  .. image:: tomato-grey.png
     :alt: grey tomato

  .. image:: tomato-purple.png
     :alt: purple tomato

  .. image:: tomato-red.png
     :alt: red tomato

  .. image:: tomato-white.png
     :alt: white tomato

  .. image:: tomato-yellow.png
     :alt: yellow tomato

-----------
Ingredients
-----------

  .. image:: ansible.png
     :target: https://www.ansible.com/
     :alt: ansible

  .. image:: apt.png
     :target: https://wiki.debian.org/Apt
     :alt: apt

  .. image:: bash.png
     :target: https://www.gnu.org/software/bash
     :alt: bash

  .. image:: debian.png
     :target: https://www.debian.org
     :alt: debian

  .. image:: docker.png
     :target: https://www.docker.com
     :alt: docker

  .. image:: git.png
     :target: https://git-scm.com
     :alt: git

  .. image:: grafana.png
     :target: https://grafana.com/
     :alt: grafana

  .. image:: graphite.png
     :target: https://graphiteapp.org/
     :alt: graphite

  .. image:: mosquitto.png
     :target: https://mosquitto.org/
     :alt: mosquitto

  .. image:: nginx.png
     :target: https://nginx.org/
     :alt: nginx

  .. image:: nodered.png
     :target: https://nodered.org/
     :alt: nodered

  .. image:: python.png
     :target: https://www.python.org
     :alt: python

  .. image:: qemu.png
     :target: https://www.qemu.org
     :alt: qemu

  .. image:: raspbian.png
     :target: https://www.raspbian.org/
     :alt: raspbian

  .. image:: reestructuredtext.png
     :target: http://docutils.sourceforge.net/rst.html
     :alt: reestructuredtext

  .. image:: sphinx.png
     :target: http://www.sphinx-doc.org
     :alt: sphinx

License
=======

GPL 3. See the `LICENSE <https://gitlab.com/huertico/repo-images/blob/master/LICENSE>`_ file for more details.

Authors Information
===================

This repository was created by `constrict0r <https://geekl0g.wordpress.com/author/constrict0r/>`_ and `valarauco <https://twitter.com/valarauco>`_.

  .. image:: constrict0r.png
     :target: https://geekl0g.wordpress.com/author/constrict0r
     :alt: constrict0r

  .. image:: valarauco.png
     :target: https://twitter.com/valarauco
     :alt: valarauco

Enjoy!!!

  .. image:: huertico.png
     :target: https://gitlab.com/huertico
     :alt: huertico